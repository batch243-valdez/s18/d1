// console.log("Hello World");

// Functions
	// functions in javascript are lines/blocks of code that tell our device/application to perform a certain task when called/invoke.
	// functions are mostly created to create complicated task to run several lines of codes in succession.
	// They are also used to prevent repeating lines/blocks of codes that contains the same task or function.

	// We also learned in the previous session that we can gather data from user using prompt window.

		function printInput(){
			let nickName = prompt("Enter your nickname: ");
			console.log("Hi " + nickName);
		}

		// printInput();

	// However, for some use cases, this may not be ideal
	// for other cases, function can also process data direcly passed into it instead of relying only on Global variable and prompt()

// Parameters and Arguments
		
		// Consider this function
		function printName(name){
			console.log("My name is " + name);
		}

		printName("Chris");

		printName("George");

		// You can directly pass data into the function. The function can then use that data which is referred as "name" within the function.

		// "name" is called parameter.
		// parameter acts as named variable/container that exist only inside of a function.

		// "Chris", is the information/data provided directly into the function called an argument.
		// Values passed when invoking a function are called arguments. These arguments are then stored as the parameters withing the function.

	// variables can also be passed as an arugment.
		let sampleVariable = "Edward";
		printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided within the function.

		function noParams(){
			let params = "No parameter";
			console.log(params);
		}

		noParams("With Parameter!");

		function checkDivisibilityBy8(num){
			let modulo = num%8;
			console.log("The remainder of " + num + " divided by 8 is: " + modulo);
			let isDivisibleBy8 = modulo === 0;
			// console.log("is " + num = "divisible by 8?");
			console.log(isDivisibleBy8);
		}

		checkDivisibilityBy8(8);

		checkDivisibilityBy8(17);

		// You can also do the same using the prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// Functions as Agruments
		// function parameters can also accept other functions as arguments.
		// Some complex functions use other functions as arguments to perform complicated results.
		// This will be further seen when we discuss arrays methods.

		function argumentFunction(){
			console.log("This function was passed as an arugment before the message was printed.");
		}

		function argumentFunctionTwo(){
			console.log("This function was passed as an arugment from the second argument function.");
		}

		function invokeFunction(argFunction){
			argFunction();
		}

		invokeFunction(argumentFunction);

		invokeFunction(argumentFunctionTwo);

		// Adding and removing the parentheses "()" impacts the output of JavaScript heavily.
		// When function is used with parentheses "()", it denotes invoking a function.
		// A function used without parentheses "()" is normally associated with using the function as an argument to another function.

	// Using Multiple parameters
		// Multiple "arguments"  will correspond to the number of "parameters" declared in a function in "succeeding order".

		function createFullName(firstName, middleName, lastName){
			console.log("This is First Name: " + firstName);
			console.log("This is Middle Name: " + middleName);
			console.log("This is Last Name: " + lastName);
		}

		createFullName("Juan", "Dela", "Cruz");

		// "Juan" will be stored in the parameter "firstName"

		createFullName("Juan", "Dela", "Cruz", "Jr.");

		createFullName("Juan", "Dela Cruz");

		// Using variables as arguments

		let firstName = "John",
			middleName = "Doe",
			lastName = "Smith";

		createFullName(firstName, middleName, lastName);

	// return statement
		// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked the function.

		function returnFullName(firstName, middleName, lastName){
			console.log(firstName + " " + middleName + " " + lastName);
		} 

		returnFullName("Ada", "None", "Lovelace");

		function returnName(firstName, middleName, lastName){
			return firstName + " " + middleName + " " + lastName;

			console.log(firstName);
		}

		console.log(returnName("John", "Doe", "Smith"));

		let fullName = returnName("John", "Doe", "Smith")
		console.log("This is the console.log from fullName variable:");
		console.log(fullName);

